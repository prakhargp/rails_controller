# What does controller do ?

Action Controllers are the core of a web request in Rails. They are made up of one or more actions that are executed on request and then either it renders a template or redirects to another action. An action is defined as a public method on the controller, which will automatically be made accessible to the web-server through Rails Routes.

For most conventional RESTful applications, the controller will receive the request (this is invisible to you as the developer), fetch or save data from a model, and use a view to create HTML output. If your controller needs to do things a little differently, that's not a problem, this is just the most common way for a controller to work.


A sample controller could look like this:

    class PostsController < ApplicationController
    def index
        @posts = Post.all
    end

    def create
        @post = Post.create params[:post]
        redirect_to posts_path
    end
    end        

Actions, by default, render a template in the app/views directory corresponding to the name of the controller and action after executing code in the action. For example, the index action of the PostsController would render the template app/views/posts/index.html.erb by default after populating the @posts instance variable.

Unlike index, the create action will not render a template. After performing its main purpose (creating a new post), it initiates a redirect instead. This redirect works by returning an external “302 Moved” HTTP response that takes the user to the index action.    

A controller can thus be thought of as a middleman between models and views. It makes the model data available to the view so it can display that data to the user, and it saves or updates user data to the model.


# Methods and Actions

A controller is a Ruby class which inherits from ApplicationController and has methods just like any other class. When your application receives a request, the routing will determine which controller and action to run, then Rails creates an instance of that controller and runs the method with the same name as the action.
    
    class ClientsController < ApplicationController
        def new
        end
    end

As an example, if a user goes to /clients/new in your application to add a new client, Rails will create an instance of ClientsController and call its new method. Note that the empty method from the example above would work just fine because Rails will by default render the new.html.erb view unless the action says otherwise. By creating a new Client, the new method can make a @client instance variable accessible in the view:

    def new
        @client = Client.new
    end


# Parameters

You will probably want to access data sent in by the user or other parameters in your controller actions. There are two kinds of parameters possible in a web application.

- The first are parameters that are sent as part of the URL, called query string parameters. The query string is everything after "?" in the URL. 

- The second type of parameter is usually referred to as POST data. This information usually comes from an HTML form which has been filled in by the user. It's called POST data because it can only be sent as part of an HTTP POST request.

Rails does not make any distinction between query string parameters and POST parameters, and both are available in the params hash in your controller:

    class ClientsController < ApplicationController
    # This action uses query string parameters because it gets run
    # by an HTTP GET request, but this does not make any difference
    # to the way in which the parameters are accessed. The URL for
    # this action would look like this in order to list activated
    # clients: /clients?status=activated
    def index
        if params[:status] == "activated"
        @clients = Client.activated
        else
        @clients = Client.inactivated
        end
    end

    # This action uses POST parameters. They are most likely coming
    # from an HTML form which the user has submitted. The URL for
    # this RESTful request will be "/clients", and the data will be
    # sent as part of the request body.
    def create
        @client = Client.new(params[:client])
        if @client.save
        redirect_to @client
        else
        # This line overrides the default rendering behavior, which
        # would have been to render the "create" view.
        render "new"
        end
    end
    end

All request parameters, whether they come from a GET or POST request, or from the URL, are available through the params method which returns a hash. For example, an action that was performed through /posts?category=All&limit=5 will include { "category" => "All", "limit" => "5" } in params.

It's also possible to construct multi-dimensional parameter hashes by specifying keys using brackets, such as:

    <input type="text" name="post[name]" value="david">
    <input type="text" name="post[address]" value="hyacintvej">

A request stemming from a form holding these inputs will include { "post" => { "name" => "david", "address" => "hyacintvej" } }. If the address input had been named post[address][street], the params would have included { "post" => { "address" => { "street" => "hyacintvej" } } }. There's no limit to the depth of the nesting.    


# Rendering XML and JSON data

ActionController makes it extremely easy to render XML or JSON data. If you've generated a controller using scaffolding, it would look something like this:

    class UsersController < ApplicationController
    def index
        @users = User.all
        respond_to do |format|
        format.html # index.html.erb
        format.xml  { render xml: @users }
        format.json { render json: @users }
        end
    end
    end

You may notice in the above code that we're using render xml: @users, not render xml: @users.to_xml. If the object is not a String, then Rails will automatically invoke to_xml for us.


# Filters

Filters are methods that are run "before", "after" or "around" a controller action.

Filters are inherited, so if you set a filter on ApplicationController, it will be run on every controller in your application.

"before" filters are registered via before_action. They may halt the request cycle. A common "before" filter is one which requires that a user is logged in for an action to be run. You can define the filter method this way:

    class ApplicationController < ActionController::Base
    before_action :require_login

    private

    def require_login
        unless logged_in?
        flash[:error] = "You must be logged in to access this section"
        redirect_to new_login_url # halts request cycle
        end
    end
    end

The method simply stores an error message in the flash and redirects to the login form if the user is not logged in. If a "before" filter renders or redirects, the action will not run. If there are additional filters scheduled to run after that filter, they are also cancelled.

In this example the filter is added to ApplicationController and thus all controllers in the application inherit it. This will make everything in the application require the user to be logged in in order to use it. For obvious reasons (the user wouldn't be able to log in in the first place!), not all controllers or actions should require this. You can prevent this filter from running before particular actions with skip_before_action:

    class LoginsController < ApplicationController
    skip_before_action :require_login, only: [:new, :create]
    end

Now, the LoginsController's new and create actions will work as before without requiring the user to be logged in. The :only option is used to skip this filter only for these actions, and there is also an :except option which works the other way. These options can be used when adding filters too, so you can add a filter which only runs for selected actions in the first place.

# Request Forgery Protection

Cross-site request forgery is a type of attack in which a site tricks a user into making requests on another site, possibly adding, modifying, or deleting data on that site without the user's knowledge or permission.

The first step to avoid this is to make sure all "destructive" actions (create, update, and destroy) can only be accessed with non-GET requests. If you're following RESTful conventions you're already doing this. However, a malicious site can still send a non-GET request to your site quite easily, and that's where the request forgery protection comes in. As the name says, it protects from forged requests.

The way this is done is to add a non-guessable token which is only known to your server to each request. This way, if a request comes in without the proper token, it will be denied access.

If you generate a form like this:

    <%= form_with model: @user do |form| %>
    <%= form.text_field :username %>
    <%= form.text_field :password %>
    <% end %>

You will see how the token gets added as a hidden field:

    <form accept-charset="UTF-8" action="/users/1" method="post">
    <input type="hidden"
        value="67250ab105eb5ad10851c00a5621854a23af5489"
        name="authenticity_token"/>
    <!-- fields -->
    </form>

Rails adds this token to every form that's generated using the form helpers, so most of the time you don't have to worry about it. If you're writing a form manually or need to add the token for another reason, it's available through the method form_authenticity_token:

# The Request and Response Objects

In every controller there are two accessor methods pointing to the request and the response objects associated with the request cycle that is currently in execution. The request method contains an instance of ActionDispatch::Request and the response method returns a response object representing what is going to be sent back to the client.

## The request Object

The request object contains a lot of useful information about the request coming in from the client. To get a full list of the available methods, refer to the Rails API documentation and Rack Documentation. Among the properties that you can access on this object are:

![](redirect.png)


Rails collects all of the parameters sent along with the request in the params hash, whether they are sent as part of the query string or the post body. The request object has three accessors that give you access to these parameters depending on where they came from. The query_parameters hash contains parameters that were sent as part of the query string while the request_parameters hash contains parameters sent as part of the post body. The path_parameters hash contains parameters that were recognized by the routing as being part of the path leading to this particular controller and action.

##  The response Object

The response object is not usually used directly, but is built up during the execution of the action and rendering of the data that is being sent back to the user, but sometimes - like in an after filter - it can be useful to access the response directly. Some of these accessor methods also have setters, allowing you to change their values. To get a full list of the available methods, refer to the Rails API documentation and Rack Documentation.

![](response.png)

## Renders

Action Controller sends content to the user by using one of five rendering methods. The most versatile and common is the rendering of a template. Included in the Action Pack is the Action View, which enables rendering of ERB templates. It's automatically configured. The controller passes objects to the view by assigning instance variables:

    def show
    @post = Post.find(params[:id])
    end

Which are then automatically available to the view:

    Title: <%= @post.title %>

## Redirects

Redirects are used to move from one action to another. For example, after a create action, which stores a blog entry to the database, we might like to show the user the new entry. Because we're following good DRY principles (Don't Repeat Yourself), we're going to reuse (and redirect to) a show action that we'll assume has already been created. The code might look like this:

    def create
    @entry = Entry.new(params[:entry])
    if @entry.save
        # The entry was saved correctly, redirect to show
        redirect_to action: 'show', id: @entry.id
    else
        # things didn't go so well, do something else
    end
    end

In this case, after saving our new entry to the database, the user is redirected to the show method, which is then executed. Note that this is an external HTTP-level redirection which will cause the browser to make a second request (a GET to the show action), and not some internal re-routing which calls both “create” and then “show” within one request.    

---

## Refrences

- [guides.rubyonrails.org](https://guides.rubyonrails.org/action_controller_overview.html)
- [rubydoc.info](https://www.rubydoc.info/docs/rails/4.1.7/ActionController/Base)